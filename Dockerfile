FROM docker.io/ubuntu:20.04
RUN apt-get update \
    && apt-get install -y python3-venv python3-pip python3-requests git curl jq \
    && curl -fsSL https://get.docker.com -o get-docker.sh \
    && sh get-docker.sh \
    && curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

ENV PATH /root/.local/bin:/root/.poetry/bin:${PATH}
RUN mkdir -p /root/.local/bin \
    && ln -s $(which python3) /root/.local/bin/python \
    && curl -sSL https://install.python-poetry.org | python3 -

ENV PIP_CACHE_DIR=/cache/pip

RUN poetry config experimental.new-installer false

RUN mkdir -p /opt/test
WORKDIR /opt/tests
